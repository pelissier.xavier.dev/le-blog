const http = require('http');
const fs = require('fs');

const Database = require('better-sqlite3');
const { time } = require('console');
const db = new Database('./db/data.db', { verbose: () => { } });
/*
const migration = fs.readFileSync('./sql-scripts/blogfromscratch-stucture.sql', 'utf8');
db.exec(migration);
const migrationData = fs.readFileSync('./sql-scripts/blogfromscratch-data.sql', 'utf8');
db.exec(migrationData);
*/

const server = http.createServer((req, res) => {
    if (req.url.indexOf('.css') != -1) {

        fs.readFile(__dirname + '/public/css/style.css', function (err, data) {
            if (err) console.log(err);
            res.writeHead(200, { 'Content-Type': 'text/css' });
    
            res.write(data);
            res.end();
        });
        return;
    }

    if (req.url.indexOf('.png') != -1) {

        fs.readFile(__dirname + '/images/icons8-blogger-150.png', function (err, data) {
            if (err) console.log(err);
            res.writeHead(200, { 'Content-Type': 'images/png' });
    
            res.write(data);
            res.end();
        });
        return;
    }



    // HEAD 
    let head = `<!DOCTYPE html>
    <html lang="en">
    <head>
         <link href="./public/css/style.css" rel="stylesheet">
        <title>Blog from scratches</title>
    </head>`;



    // HEADER
    let header =` <body>
    <header>  
     
        <ul>
            <li>
            <h5>  </h5>
            </li>
            <li>
            <h1>Blog from Sracth</h1>
            </li>
    
        </ul>
   
    <nav>
    <ul>
        <li class="deroulant"><a href="#">Filter by author&#9660</a>
            <ul class="sous">
                <li><a href="#">Paul Auchon</a></li>
                <li><a href="#">Gerard Manvussa</a></li>
            </ul>
        </li>
        <li class="déroulant"><a href="#">Filter by categorie&#9660</a>
            <ul class="sous">
                <li><a href="#">Language</a></li>
                <li><a href="#">Package</a></li>
            </ul>
        </li> 

    </header>
    </body>`


    //FOOTER
    let footer =` </div>
    <footer>
    2020 blog from scratches
     </footer>`
   

    //CONTENUE DIV
    

    //CONTENUE ARTCILE    

    
    const stmt = db.prepare('select * from articles limit 4').all();

    let article1 = '<div class="container">';
    stmt.forEach(article => {
        const author = db.prepare('select * from authors where id = ?').get(article.author_id);
        article1 +=  `<div class="stylearticle">
        

         <img src="${article.image_url}">  
         <h2>${article.title}</h2>
         <h2>${author.firstname} ${author.lastname}  ${article.reading_time} </h2>
         ${article.content}
         ${article.reading_time}

        
                        </div> `
    }
    ) ;
   
    console.log();
    res.statusCode=200;
    res.setHeader=('content-type','text/html; charset=utf-8');
    res.end(head + header   + article1 + footer);
});


server.listen(3000, '127.0.0.1', () => {
    console.log('Server running at http://127.0.0.1:3000/');
});